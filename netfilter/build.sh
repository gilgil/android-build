export CFLAGS=$CFLAGS' -O2'

#
# libmnl
#
# wget https://netfilter.org/projects/libmnl/files/libmnl-1.0.4.tar.bz2
tar xf libmnl-1.0.4.tar.bz2
cd libmnl-1.0.4
./configure --prefix=$PWD/../sysroot --host=arm-linux --enable-shared=no
make
make install
cd ..

#
# libnfnetlink
#
# wget https://netfilter.org/projects/libnfnetlink/files/libnfnetlink-1.0.1.tar.bz2
tar xf libnfnetlink-1.0.1.tar.bz2
cd libnfnetlink-1.0.1
./configure --prefix=$PWD/../sysroot --host=arm-linux --enable-shared=no
make
make install
cd ..

#
# libnetfilter_queue
#
# wget https://netfilter.org/projects/libnetfilter_queue/files/libnetfilter_queue-1.0.3.tar.bz2
tar xf libnetfilter_queue-1.0.3.tar.bz2
cd libnetfilter_queue-1.0.3
export CFLAGS=$CFLAGS' -fPIC -I../../sysroot/include' # for libmnl.h and libnfnetlink.h in src folder
export LDFLAGS=$LDFLAGS' -fPIC' # for remove warning requires unsupported dynamic reloc R_ARM_REL32; recompile with -fPIC
export PKG_CONFIG_PATH='../sysroot/lib/pkgconfig' # for libmnl and libnfnetlink
./configure --prefix=$PWD/../sysroot --host=arm-linux --enable-shared=no
make
make install
cd ..

echo --------------------------------------------------------------------------
echo copy all sysroot files into $ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/sysroot
echo --------------------------------------------------------------------------

