android profile를 설정한다.
```
$ source android.profile
```

build.sh 파일을 실행하여 netfilter library를 빌드한다. libnetfilter_queue는 libmnl과 libnfnetlink에 대해 dependency를 가진다. 그러므로 libmnl 및 libnfnetlink를 먼저 빌드한다.
```
$ ./build.sh
```

make install까지 완료되면 sysroot 폴더에 include, lib 폴더가 생긴다. test 폴더로 이동하여 netfilter_queue 사용을 테스트해 본다.
```
$ cd test
$ make
$ adb push nfqnl-test /data/local/tmp
$ adb shell

$ su
$ cd /data/local/tmp
$ iptables -A OUTPUT -j NFQUEUE
$ ./nfqnl-test
$ iptables -D OUTPUT -j NFQUEUE
```

android 개발 환경에서 make를 실행할 때 컴파일 에러가 나는 경우 다음과 같이 수정한다.

### redefinition of 'nfnetlink_groups'
```
In file included from nlmsg.c:24:
In file included from ../include/libnetfilter_queue/libnetfilter_queue.h:17:
In file included from /root/proj/g/android/build/netfilter/libnfnetlink-1.0.1/../sysroot/include/libnfnetlink/libnfnetlink.h:20:
/root/proj/g/android/build/netfilter/libnfnetlink-1.0.1/../sysroot/include/libnfnetlink/linux_nfnetlink.h:6:6: error: redefinition of 'nfnetlink_groups'
enum nfnetlink_groups {
     ^
/root/android/ndk/toolchains/llvm/prebuilt/linux-x86_64/bin/../sysroot/usr/include/linux/netfilter/nfnetlink.h:23:6: note: previous definition is here                                                                                     
enum nfnetlink_groups {
     ^
```

src/nlmsg.c 파일에서 libnetfilter_queue.h 파일을 include하지 않도록 주석을 단다(24 line).
```
#include <linux/netfilter/nfnetlink_queue.h>

// #include <libnetfilter_queue/libnetfilter_queue.h> // by gilgil
```

### redefinition of 'tcp_word_hdr'
```
src/extra/tcp.c:122:7: error: redefinition of 'tcp_word_hdr'                                                                                                                                                                                   
union tcp_word_hdr {
      ^
```

src/extra/tcp.c 파일에서 해당 union 선언을 삭제한다(122 line).
```
// ----- by gilgil
/*
union tcp_word_hdr {
	struct tcphdr hdr;
	uint32_t  words[5];
};
*/
```

### incomplete definition of type 'struct ethhdr'
```
src/extra/pktbuff.c:73:16: error: incomplete definition of type 'struct ethhdr'
                switch(ethhdr->h_proto) {
                       ~~~~~~^
```

src/extra/pktbuff.c 파일에서 올바른 if_ether.h 파일을 include 한다(16 line).
```
// #include <netinet/if_ether.h> // by gilgil
#include <linux/if_ether.h> // by gilgil
```
