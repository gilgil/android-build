cpp로 제작하는 경우 기본적으로 libc++_shared.so 파일을 필요로 한다. 이를 위해서 적당한 libc++_shared.so 파일을 Android 기기에 복사해 줘야 한다.


### 실행 파일과 같은 폴더에 so 파일을 놓고 실행하기
so 파일과 실행 파일을 같은 폴더에 놓고 다음과 같이 LD_LIBRARY_PATH를 설정하여 실행할 수 있다.
```
$ export LD_LIBRARY_PATH=.; ./hello-world
```

### 시스템 폴더에 so 파일 복사하기

so 파일을 /system/lib 에 놓으면 어디에서나 이용할 수 있다. 그런데 시스템 폴더는 기본적으로 read only이기 때문에 so 파일을 그곳에 복사할 수 없다.

```
$ cp libc++_shared.so /system/lib
cp: can't create '/system/lib/libc++_shared.so': Read-only file system
```
이 경우 다음과 같은 명령어로 system 폴더에 write 권한을 줄 수 있다.

```
$ mount -o rw,remount /system
```

복사가 이루어지고난 이후 chmod 명령어를 통하여 읽기 권한을 변경하도록 한다.
```
$ chmod 644 /system/lib/libc++_shared.so
```

### 시스템 폴더에 so 파일 복사 예제
```
$ adb push /root/android/ndk//toolchains/llvm/prebuilt/linux-x86_64/sysroot/usr/lib/arm-linux-androideabi/libc++_shared.so data/local/tmp
```

```
$ adb shell

$ su
$ cd /data/local/tmp
$ mount -o rw,remount /system
$ cp libc++_shared.so /system/lib
$ chmod 644 /system/lib/libc++_shared.so
```
