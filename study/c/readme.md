Android로 빌드하기 위해서는 다음과 같이 하면 된다.

1. clang과 clang++이 설치되어 있는 위치를 PATH에 추가한다.

2. target 옵션을 추가한다.

1번(PATH)과 2번(target 옵션)은 전부 Qt Creator에서 Android용으로 임의의 프로젝틀를 빌드를 할 때 Compile Output 탭에 나오는 로그를 보면 알 수 있다.

### x64용 빌드

그냥 make를 때리면 다음과 같이 나온다. 실행 파일은 x64에서 실행될 수 있는 파일이다.

```
$ source x64.profile
$ make clean
$ make
$ file hello-world
hello-world: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=c44dd8e67ab61d0f1527ef033acc3866cb5533b4, for GNU/Linux 3.2.0, not stripped
```

### android용 빌드
Android용으로 빌드하기 위해서는 "source android.profile" 명령어를 통하여 환경을 설정한 이후 다시 make를 때리면 된다. file 명령어 결과를 보면 ARM용(Android용)으로 빌드된 것임을 알 수 있다.
```
$ source android.profile
$ make clean
$ make
$ file hello-world
hello-world: ELF 32-bit LSB shared object, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /system/bin/linker, not stripped
```

### android 실행 테스트
실행 파일을 /data/local/tmp에 복사를 해서 실행이 되는 것을 확인한다.

```
$ adb push hello-world /data/local/tmp
$ adb shell

$ cd /data/local/tmp
$ ./hello-world
hello world
```
