### non defined
|Symbol|Description|Example|
|-|-|-|
|CC|C compiler|cc|
|CXX|C++ compiler|g++|
|CFLAGS|C compile and link options|-O2 -I../include -g|
|CXXFLAGS|C++ compile and link options||
|CPPFLAGS|C, C++ compile and link options||
|LDFLAGS|Link options|-L../lib|
|LDLIBS|Link symbols|-lpcap|
|TARGET_ARCH|Architecture options|-target armv7a-linux-androideabi23|


### default

55
```
OUTPUT_OPTION = -o $@
```

57
```
COMPILE.cpp = $(COMPILE.cc)
```

71
```
CC = cc
```

79
```
LINK.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
```

147
```
LINK.C = $(LINK.cc)
```

209
```
LINK.cpp = $(LINK.cc)
```

229
```
LINK.o = $(CC) $(LDFLAGS) $(TARGET_ARCH)
```

259
```
CXX = g++
```

255
```
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
```

275
```
COMPILE.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
```

### compile

339
```
%.o: %.c
	$(COMPILE.c) $(OUTPUT_OPTION) $<
```

349
```
%.o: %.cc
	$(COMPILE.cc) $(OUTPUT_OPTION) $<
```

369
```
%.o: %.cpp
	$(COMPILE.cpp) $(OUTPUT_OPTION) $<
```

### link

325
```
%: %.o
	$(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@
```

331
```
%: %.c
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@
```

345
```
%: %.cc
	$(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@
```

365
```
%: %.cpp
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@
```
