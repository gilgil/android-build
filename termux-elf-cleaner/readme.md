## Install
```
sudo apt install automake

git clone https://github.com/termux/termux-elf-cleaner.git

cd termux-elf-cleaner

git checkout v2.2.1 # latest release version

aclocal # create aclocal.m4 and autom4te.cache/

# autoheader

automake --force-missing --add-missing

autoconf

./configure

make

sudo make install
```

## Usage
```
termux-elf-cleaner --api-level 23 hello-world

```
