#
# '/root' directory should be changed according to user settings
#
export ANDROID_NDK_ROOT=/root/Android/Sdk/ndk/25.1.8937393

function addPath {
  case ":$PATH:" in
    *":$1:"*) :;; # already there
    *) PATH="$1:$PATH";; # or PATH="$PATH:$1"
  esac
}

addPath $ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/bin

export CC=clang
export CXX=clang++
export CFLAGS="-target armv7a-linux-androideabi23"
export CXXFLAGS="-target armv7a-linux-androideabi23"
export LDFLAGS="-target armv7a-linux-androideabi23"

