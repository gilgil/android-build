export CFLAGS=$CFLAGS' -O2'

#wget http://www.tcpdump.org/release/libpcap-1.9.1.tar.gz
tar xf libpcap-1.9.1.tar.gz
cd libpcap-1.9.1

./configure --prefix=$PWD/../sysroot --host=arm-linux --enable-shared=no --with-pcap=linux --without-libnl 
make
make install
cd ..

echo --------------------------------------------------------------------------
echo copy all sysroot files into $ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/sysroot
echo --------------------------------------------------------------------------
