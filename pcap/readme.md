deps.sh 파일을 실행하여 dependency를 설정한다.
```
$ ./deps.sh
```

android profile를 설정한다.
```
$ source android.profile
```

build.sh 파일을 실행하여 pcap library를 빌드한다.
```
$ ./build.sh
```

make install까지 완료되면 sysroot 폴더에 include, lib 폴더가 생긴다. test 폴더로 이동하여 pcap 사용을 테스트해 본다.
```
$ cd test
$ make
$ adb push interface-list /data/local/tmp
$ adb shell

$ su
$ cd /data/local/tmp
$ ./interface-list
```
