Android build for clang
===

* netfilter : netfilter buid
* netfilter/test : netfilter usage example
* pcap : pcap build
* pcap/test : pcap usage example
* qt : Qt samples
* qt/c : Non-Qt Project > Plain C Application
* qt/cpp : Non-Qt Project > Plain C++ Application
* study : usage of android.profile x64.profile
* study/c : C example
* study/cpp : C++ example
* study/makefile : makefile options
